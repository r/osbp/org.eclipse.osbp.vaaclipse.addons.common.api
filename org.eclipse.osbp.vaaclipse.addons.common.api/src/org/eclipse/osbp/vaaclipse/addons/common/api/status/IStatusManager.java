/**
 * Copyright (c) 2011, 2014 - Lunifera GmbH (Gross Enzersdorf)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors: 
 * 		Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.vaaclipse.addons.common.api.status;

import java.util.List;

import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.osbp.runtime.common.validation.IStatus;

/**
 * Collects status about errors and warnings. A status manager is attached to a
 * Vaaclipse session.
 */
public interface IStatusManager {

	/**
	 * IStatus property key for the original java.validation constraint.
	 */
	public static final String PROP_JAVAX_VALIDATION_CONSTRAINT = "statusmanager.javax.validation.constraint";

	/**
	 * Returns a immutable list with all scopes registered within the status
	 * manager.
	 *
	 * @return the all scopes
	 */
	List<IStatusScope> getAllScopes();

	/**
	 * Returns the scope of the active MPart.<br>
	 * Returns a DefaultScope if no MPart is active.
	 *
	 * @return the active scope
	 */
	IStatusScope getActiveScope();

	/**
	 * Returns the scope of the given MPart.
	 *
	 * @param mPart
	 *            the m part
	 * @return the scope for
	 */
	IStatusScope getScopeFor(MPart mPart);

	/**
	 * Returns a immutable list with all status of the active MPart. <br>
	 * Returns <code>null</code> if no MPart is active.
	 *
	 * @return the all status
	 */
	List<IStatus> getAllStatus();
	
	/**
	 * Returns a immutable list with all status of all scopes. <br>
	 * Returns <code>null</code> if no scope is available.
	 *
	 * @return the all scope status
	 */
	List<IStatus> getAllScopeStatus();

}