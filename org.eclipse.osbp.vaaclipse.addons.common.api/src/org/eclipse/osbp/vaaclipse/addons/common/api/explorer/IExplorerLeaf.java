/**
 * Copyright (c) 2011, 2014 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors: 
 * 		Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.vaaclipse.addons.common.api.explorer;

import org.eclipse.e4.core.contexts.IEclipseContext;

/**
 * The Interface IExplorerLeaf.
 */
public interface IExplorerLeaf extends IExplorerInfo {

	/**
	 * Executes the info. This method may open a view or also do nothing.
	 *
	 * @param eclipseContext
	 *            the eclipse context
	 */
	void execute(IEclipseContext eclipseContext);
}
