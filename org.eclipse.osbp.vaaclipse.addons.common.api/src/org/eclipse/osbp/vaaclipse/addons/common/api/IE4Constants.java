/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.vaaclipse.addons.common.api;

/**
 * The Interface IE4Constants.
 */
public interface IE4Constants {

//	/**
//	 * The instance of the ui action. For instance YExposedAction from ECView,
//	 * or any other action for XWT,...
//	 */
//	public static final String PARAM_ACTION = "uiActionInstance";
//
//	/**
//	 * The class which should be used as key, to register the
//	 * {@link #PARAM_ACTION} in the eclipse context.
//	 */
//	public static final String PARAM_ACTION_TYPE_KEY = "uiActionKeyType";

	/**
	 * The id of the ext save command. (ext means that the enablement is controlled by the view)
	 */
	public static final String COMMAND_SAVE = "org.eclipse.osbp.vaaclipse.addons.application.command.extsave";

	/**
	 * The id of the ext delete command. (ext means that the enablement is controlled by the view)
	 */
	public static final String COMMAND_DELETE = "org.eclipse.osbp.vaaclipse.addons.application.command.extdelete";

	/**
	 * The id of the ext default part callback command. (ext means that the enablement is controlled by the view)
	 */
	public static final String COMMAND_DEFAULT_PART_CALLBACK = "org.eclipse.osbp.vaaclipse.addons.application.command.extdefault";

	/**
	 * The id of the ext load command. (ext means that the enablement is controlled by the view)
	 */
	public static final String COMMAND_LOAD = "org.eclipse.osbp.vaaclipse.addons.application.command.extload";

	/** The Constant PROP_INPUT_VIEW_ID. */
	public static final String PROP_INPUT_VIEW_ID = "viewId";
	
	/** The Constant BUNDLECLASS_GENERIC_ECVIEW_VIEWPART. */
	public static final String BUNDLECLASS_GENERIC_ECVIEW_VIEWPART = "bundleclass://org.eclipse.osbp.vaaclipse.addons.ecview/org.eclipse.osbp.vaaclipse.addons.ecview.views.GenericECViewPart";
	
	/** The Constant ID__PARTSTACK__LEFT. */
	public static final String ID__PARTSTACK__LEFT = "org.eclipse.osbp.vaaclipse.addons.application.partstack.left";
	
	/** The Constant ID__PARTSTACK__RIGHT. */
	public static final String ID__PARTSTACK__RIGHT = "org.eclipse.osbp.vaaclipse.addons.application.partstack.right";
	
	/** The Constant ID__PARTSTACK__MAIN. */
	public static final String ID__PARTSTACK__MAIN = "org.eclipse.osbp.vaaclipse.addons.application.partstack.main";
	
	/** The Constant ID__PARTSTACK__BOTTOM. */
	public static final String ID__PARTSTACK__BOTTOM = "org.eclipse.osbp.vaaclipse.addons.application.partstack.bottom";
	
	/** The Constant ID__PARTSTACK__MASTER_DETAIL__MASTER. */
	public static final String ID__PARTSTACK__MASTER_DETAIL__MASTER = "org.eclipse.osbp.vaaclipse.addons.application.partstack.materdetail.master";
	
	/**
	 * Used as property in OSGi services.
	 */
	public static final String APPLICATION_ID = "e4.application.id";

	/** The source element from the keystroke event. */
	public static final String KEYSTROKE_SOURCE = "keystroke.source";
	
	/** The target element from the keystroke event. */
	public static final String KEYSTROKE_TARGET = "keystroke.target";
	
	
}
