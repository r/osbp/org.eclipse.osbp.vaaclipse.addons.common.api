/**
 * Copyright (c) 2011, 2014 - Lunifera GmbH (Gross Enzersdorf)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors: 
 * 		Florian Pirchner - Initial implementation
 */
 package org.eclipse.osbp.vaaclipse.addons.common.api.status;

import java.util.Collection;
import java.util.List;

import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.osbp.runtime.common.validation.IStatus;

/**
 * The scope is responsible to collect IStatus objects for a single part.
 */
public interface IStatusScope {

	/**
	 * Returns the MPart this scope is attached to.
	 *
	 * @return the m part
	 */
	MPart getMPart();

	/**
	 * Adds a new status to the list of status.
	 *
	 * @param status
	 *            the status
	 */
	void addStatus(IStatus status);

	/**
	 * Adds all given status to the list of status.
	 *
	 * @param status
	 *            the status
	 */
	void addAllStatus(Collection<IStatus> status);

	/**
	 * Removes the given status.
	 *
	 * @param status
	 *            the status
	 */
	void removeStatus(IStatus status);

	/**
	 * Removes all given status from the list of status.
	 *
	 * @param status
	 *            the status
	 */
	void removeAllStatus(Collection<IStatus> status);

	/**
	 * Removes the old status and adds the new status. The change event is only
	 * sent once.
	 *
	 * @param oldStatus
	 *            the old status
	 * @param newStatus
	 *            the new status
	 */
	void modifyStatus(Collection<IStatus> oldStatus,
			Collection<IStatus> newStatus);

	/**
	 * Removes all collected status.
	 */
	void clearStatus();

	/**
	 * Returns a list with all status objects.
	 *
	 * @return the all status
	 */
	List<IStatus> getAllStatus();

}
