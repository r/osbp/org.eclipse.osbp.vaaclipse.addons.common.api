/**
 * Copyright (c) 2011, 2014 - Lunifera GmbH (Gross Enzersdorf)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors: 
 * 		Florian Pirchner - Initial implementation
 */

 package org.eclipse.osbp.vaaclipse.addons.common.api.di;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Parts can specify this annotation on one of the methods to tag it as the
 * method that returns "enabled" states for commands.
 * <p>
 * This annotation must not be applied to more than one method on a class. If
 * multiple methods of the class are tagged with this this annotation, only one
 * of them will be called.
 * </p>
 * 
 * @since 1.0
 */
@Documented
@Target({ ElementType.METHOD })
@Retention(RetentionPolicy.RUNTIME)
public @interface EnabledState {
	// intentionally left empty
}
