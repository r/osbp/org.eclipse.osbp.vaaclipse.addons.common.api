/**
 * Copyright (c) 2011, 2014 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors: 
 * 		Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.vaaclipse.addons.common.api.help;

import java.util.Map;

import org.eclipse.e4.core.contexts.IEclipseContext;

/**
 * The Interface IHelpViewService.
 */
public interface IHelpViewService {

	/**
	 * Opens the help view related to the field for the given information.
	 *
	 * @param fieldId
	 *            the field id
	 * @param partContext
	 *            the part context
	 */
	void openFieldHelp(String fieldId, IEclipseContext partContext);

	/**
	 * Opens the help view related to the part for the given information.
	 *
	 * @param partId
	 *            the part id
	 * @param properties
	 *            the properties
	 */
	void openPartHelp(String partId, Map<String, Object> properties);

}
