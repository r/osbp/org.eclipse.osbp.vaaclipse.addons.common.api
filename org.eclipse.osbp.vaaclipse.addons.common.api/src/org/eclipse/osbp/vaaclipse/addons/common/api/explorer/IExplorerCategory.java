/**
 * Copyright (c) 2011, 2014 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors: 
 * 		Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.vaaclipse.addons.common.api.explorer;

import java.util.List;

/**
 * The Interface IExplorerCategory.
 */
public interface IExplorerCategory extends IExplorerInfo {

	
	/**
	 * Returns the parent category of this category or <code>null</code> if no
	 * parent category exists.
	 *
	 * @return the parent category
	 */
	IExplorerCategory getParentCategory();

	/**
	 * Returns a unmodifiable list of children.
	 *
	 * @return the children
	 */
	List<IExplorerInfo> getChildren();

}
