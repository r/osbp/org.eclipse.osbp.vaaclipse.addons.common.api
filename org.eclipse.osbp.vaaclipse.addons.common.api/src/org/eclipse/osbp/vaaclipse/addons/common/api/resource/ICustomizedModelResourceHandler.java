/**
 * Copyright (c) 2011, 2014 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors: 
 * 		Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.vaaclipse.addons.common.api.resource;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import org.eclipse.e4.ui.model.application.ui.advanced.MPerspective;
import org.eclipse.e4.ui.workbench.IModelResourceHandler;

/**
 * The Interface ICustomizedModelResourceHandler.
 */
public interface ICustomizedModelResourceHandler extends IModelResourceHandler {

	/** The Constant SYSTEM_USER. */
	public static final String SYSTEM_USER = "systemUser";

	/**
	 * Loads the perspective for the given input stream.
	 *
	 * @param stream
	 *            the stream
	 * @return the m perspective
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	MPerspective loadPerspective(InputStream stream) throws IOException;

	/**
	 * Saves the perspective.
	 *
	 * @param perspective
	 *            the perspective
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	void savePerspective(MPerspective perspective) throws IOException;
	
	/**
	 * Saves the perspective in the stream.
	 *
	 * @param stream
	 *            the stream
	 * @param perspective
	 *            the perspective
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	void savePerspective(OutputStream stream, MPerspective perspective) throws IOException;
}
