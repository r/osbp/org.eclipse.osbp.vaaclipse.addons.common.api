/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.vaaclipse.addons.common.api;

import org.eclipse.osbp.runtime.common.validation.IStatus;

/**
 * The Interface IE4Topics.
 */
public interface IE4Topics {

	/**
	 * The Class StatusManagerEvents.
	 */
	public static class StatusManagerEvents {
		/**
		 * An event is sent to this topic, if the active scope was changed.
		 */
		public static final String ACTIVE_SCOPE_CHANGED_TOPIC = "org/eclipse/osbp/vaaclipse/statusmanager/activescope/changed";

		/**
		 * An event is sent to this topic, if validations have been added or
		 * removed to IStatusManager.
		 */
		public static final String VALIDATIONS_CHANGED_TOPIC = "org/eclipse/osbp/vaaclipse/statusmanager/validations/changed";

		/** The Constant PROP_SCOPE. */
		public static final String PROP_SCOPE = "scope";

		/** The Constant PROP_STATUS. */
		public static final String PROP_STATUS = "status";
	}

	/**
	 * The Class PartEvents.
	 */
	public static class PartEvents {
		/**
		 * An event is sent to this topic, if an MPart should focus a field.
		 */
		public static final String FOCUS_FIELD_TOPIC = "org/eclipse/osbp/vaaclipse/parts/fields/focus";

		/**
		 * The id of the mpart.
		 */
		public static final String PROP_MPART_ID =  IStatus.PROP_UI_APPLICATION_ID;

		/**
		 * The id of the field.
		 */
		public static final String PROP_FIELD_ID = IStatus.PROP_FIELD_ID;

		/**
		 * I18nKey of the field.
		 */
		public static final String PROP_FIELD_I18N_KEY = IStatus.PROP_FIELD_I18N_KEY;
	}
	
	public static class ToolBarEvents {
		public enum States {ADD_ENTRY, MODIFY_ENTRY, CLEAN, DIRTY, VALID, INVALID;}
		public enum Labels {MODE, STATE, VALIDITY;}
		
		public static final String STATE_LABEL = "label";
		public static final String STATE_STATE = "state";
		public static final String STATE_UUID = "uuid";
		
		/** The Constant NOTIFY_TOOLBAR_DIALOG_STATE to send the dialog's state to a toolbar label (unmodified,dirty,invalid,valid, etc). */
		public static final String NOTIFY_TOOLBAR_DIALOG_STATE = "org/eclipse/osbp/vaaclipse/toolbarevents/dialog/state";
	}

}
