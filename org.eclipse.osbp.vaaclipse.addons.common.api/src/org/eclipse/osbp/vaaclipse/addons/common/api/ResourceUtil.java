/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.vaaclipse.addons.common.api;

import org.eclipse.osbp.vaaclipse.publicapi.resources.BundleResource;
import org.osgi.framework.Bundle;
import org.osgi.framework.FrameworkUtil;

import com.vaadin.server.ExternalResource;
import com.vaadin.server.Resource;
import com.vaadin.server.ThemeResource;

/**
 * A util that deals with vaadin resources.
 */
public class ResourceUtil {

	/**
	 * Returns the proper resource for the given uri.
	 *
	 * @param iconURI
	 *            the icon uri
	 * @return the resource
	 */
	public static Resource getResource(String iconURI) {
		if (iconURI.startsWith("platform:/plugin/")) {
			return BundleResource.valueOf(iconURI);
		} else if (iconURI.startsWith("bundleentry:/")) {
			return BundleResource.valueOf(bundleEntryToPlatform(iconURI));
		} else if (iconURI.startsWith("bundleresource:/")) {
			return BundleResource.valueOf(bundleResourceToPlatform(iconURI));
		} else if (iconURI.startsWith("theme:/")) {
			return new ThemeResource(iconURI.replace("theme:/", ""));
		} else if (iconURI.startsWith("http")) {
			return new ExternalResource(iconURI);
		}

		return new ThemeResource(iconURI);
	}

	/**
	 * Bundle entry to platform.
	 *
	 * @param iconURI
	 *            the icon uri
	 * @return the string
	 */
	// eg bundleentry://632.fwk284268103/icons/full/obj16/Perspective.gif
	private static String bundleEntryToPlatform(String iconURI) {
		StringBuilder b = new StringBuilder("platform:/plugin/");

		String temp = iconURI.substring(14);
		String bundleId = temp.substring(0, temp.indexOf("."));
		String path = temp.substring(temp.indexOf("/") + 1, temp.length());

		Bundle bundle = FrameworkUtil.getBundle(ResourceUtil.class);
		Bundle target = bundle.getBundleContext().getBundle(
				Long.valueOf(bundleId));
		String bsn = target.getSymbolicName();

		b.append(bsn);
		b.append("/");
		b.append(path);

		return b.toString();
	}

	/**
	 * Bundle resource to platform.
	 *
	 * @param iconURI
	 *            the icon uri
	 * @return the string
	 */
	// eg bundleresource://20950.fwk865744496/icons/full/obj16/JvmParameterizedTypeReference.gif
	private static String bundleResourceToPlatform(String iconURI) {
		StringBuilder b = new StringBuilder("platform:/plugin/");

		String temp = iconURI.substring(17);
		String bundleId = temp.substring(0, temp.indexOf("."));
		String path = temp.substring(temp.indexOf("/") + 1, temp.length());

		Bundle bundle = FrameworkUtil.getBundle(ResourceUtil.class);
		Bundle target = bundle.getBundleContext().getBundle(
				Long.valueOf(bundleId));
		String bsn = target.getSymbolicName();

		b.append(bsn);
		b.append("/");
		b.append(path);

		return b.toString();
	}

}
