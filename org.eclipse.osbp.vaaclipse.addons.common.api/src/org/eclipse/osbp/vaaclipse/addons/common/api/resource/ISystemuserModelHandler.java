/**
 * Copyright (c) 2011, 2014 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors: 
 * 		Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.vaaclipse.addons.common.api.resource;

import java.io.IOException;

import org.eclipse.e4.ui.model.application.ui.advanced.MPerspective;
import org.eclipse.e4.ui.model.application.ui.advanced.MPerspectiveStack;
import org.eclipse.e4.ui.model.fragment.MModelFragments;

/**
 * Handles the fragment for the system user. This fragment is used to persist
 * and load common settings for all users.
 */
public interface ISystemuserModelHandler {

	/**
	 * Persists the fragment.
	 *
	 * @param mFragments
	 *            the m fragments
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public abstract void persistFragment(MModelFragments mFragments)
			throws IOException;

	/**
	 * Process the given fragment.
	 */
	public abstract void mergeFragment();

	/**
	 * Creates a new MModelFragment, configures it properly and adds it to the
	 * fragments container.
	 *
	 * @param parent
	 *            the parent
	 * @param newPerspective
	 *            the new perspective
	 */
	public abstract void addPerspectiveFragment(
			MPerspectiveStack parent, MPerspective newPerspective);

	/**
	 * Removes the fragment for the given perspective from the container.
	 *
	 * @param parentId
	 *            the parent id
	 * @param perspective
	 *            the perspective
	 */
	public abstract void removePerspectiveFragment(String parentId,
			MPerspective perspective);

}