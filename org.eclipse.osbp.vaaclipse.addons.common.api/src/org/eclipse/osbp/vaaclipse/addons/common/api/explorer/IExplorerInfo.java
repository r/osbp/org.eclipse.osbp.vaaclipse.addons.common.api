/**
 * Copyright (c) 2011, 2014 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors: 
 * 		Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.vaaclipse.addons.common.api.explorer;

import com.vaadin.server.Resource;

/**
 * The Interface IExplorerInfo.
 */
public interface IExplorerInfo extends Comparable<IExplorerInfo> {

	/** The Constant PROP__LABEL. */
	public static final String PROP__LABEL = "label";
	
	/** The Constant PROP__DESCRIPTION. */
	public static final String PROP__DESCRIPTION = "description";
	
	/** The Constant PROP__ICON. */
	public static final String PROP__ICON = "icon";

	/**
	 * Returns the unique ID of the info.
	 *
	 * @return the id
	 */
	String getId();

	/**
	 * Returns the target information of the info. May be <code>null</code>. For
	 * views that will be the original view id. And for a category it will be
	 * the original package it represents.
	 *
	 * @return the target
	 */
	String getTarget();

	/**
	 * Returns the category of the info.
	 *
	 * @return the category
	 */
	IExplorerCategory getCategory();

	/**
	 * Returns the icon.
	 *
	 * @return the icon i18n key
	 */
	String getIconI18nKey();

	/**
	 * Returns the label key for i18n translations.
	 *
	 * @return the i18n label key
	 */
	String getI18nLabelKey();

	/**
	 * Returns the description key for i18n translations.
	 *
	 * @return the i18n description key
	 */
	String getI18nDescriptionKey();

	/**
	 * Returns the icon.
	 *
	 * @return the icon
	 */
	Resource getIcon();

	/**
	 * Returns the icon uri.
	 *
	 * @return the icon uri
	 */
	String getIconURI();

	/**
	 * Returns the label.
	 *
	 * @return the label
	 */
	String getLabel();

	/**
	 * Returns the description.
	 *
	 * @return the description
	 */
	String getDescription();
}
