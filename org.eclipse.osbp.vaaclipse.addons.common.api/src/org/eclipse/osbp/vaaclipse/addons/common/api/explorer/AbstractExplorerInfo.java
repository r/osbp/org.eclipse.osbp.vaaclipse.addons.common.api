/**
 * Copyright (c) 2011, 2014 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors: 
 * 		Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.vaaclipse.addons.common.api.explorer;

import org.eclipse.osbp.vaaclipse.addons.common.api.ResourceUtil;

import com.vaadin.server.Resource;

/**
 * The Class AbstractExplorerInfo.
 */
public abstract class AbstractExplorerInfo implements IExplorerInfo {

	/** The id. */
	private String id;
	
	/** The target. */
	private String target;
	
	/** The category. */
	private IExplorerCategory category;
	
	/** The icon key. */
	private String iconKey;
	
	/** The i18n label key. */
	private String i18nLabelKey;
	
	/** The i18n description key. */
	private String i18nDescriptionKey;
	
	/** The icon. */
	private Resource icon;
	
	/** The label. */
	private String label;
	
	/** The description. */
	private String description;
	
	/** The icon uri. */
	private String iconURI;

	/* (non-Javadoc)
	 * @see org.eclipse.osbp.vaaclipse.addons.common.api.explorer.IExplorerInfo#getId()
	 */
	@Override
	public String getId() {
		return id;
	}

	/* (non-Javadoc)
	 * @see org.eclipse.osbp.vaaclipse.addons.common.api.explorer.IExplorerInfo#getTarget()
	 */
	@Override
	public String getTarget() {
		return target;
	}

	/* (non-Javadoc)
	 * @see org.eclipse.osbp.vaaclipse.addons.common.api.explorer.IExplorerInfo#getCategory()
	 */
	@Override
	public IExplorerCategory getCategory() {
		return category;
	}

	/* (non-Javadoc)
	 * @see org.eclipse.osbp.vaaclipse.addons.common.api.explorer.IExplorerInfo#getIconI18nKey()
	 */
	@Override
	public String getIconI18nKey() {
		return iconKey;
	}

	/* (non-Javadoc)
	 * @see org.eclipse.osbp.vaaclipse.addons.common.api.explorer.IExplorerInfo#getI18nLabelKey()
	 */
	@Override
	public String getI18nLabelKey() {
		return i18nLabelKey;
	}

	/* (non-Javadoc)
	 * @see org.eclipse.osbp.vaaclipse.addons.common.api.explorer.IExplorerInfo#getI18nDescriptionKey()
	 */
	@Override
	public String getI18nDescriptionKey() {
		return i18nDescriptionKey;
	}

	/* (non-Javadoc)
	 * @see org.eclipse.osbp.vaaclipse.addons.common.api.explorer.IExplorerInfo#getIconURI()
	 */
	@Override
	public String getIconURI() {
		return iconURI;
	}

	/* (non-Javadoc)
	 * @see org.eclipse.osbp.vaaclipse.addons.common.api.explorer.IExplorerInfo#getIcon()
	 */
	@Override
	public Resource getIcon() {
		if (icon == null && iconURI != null && !iconURI.equals("")) {
			icon = ResourceUtil.getResource(iconURI);
		}
		return icon;
	}

	/* (non-Javadoc)
	 * @see org.eclipse.osbp.vaaclipse.addons.common.api.explorer.IExplorerInfo#getLabel()
	 */
	@Override
	public String getLabel() {
		return label != null ? label : "";
	}

	/* (non-Javadoc)
	 * @see org.eclipse.osbp.vaaclipse.addons.common.api.explorer.IExplorerInfo#getDescription()
	 */
	@Override
	public String getDescription() {
		return description;
	}

	/**
	 * Sets the id.
	 *
	 * @param id
	 *            the new id
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * Sets the target.
	 *
	 * @param target
	 *            the new target
	 */
	public void setTarget(String target) {
		this.target = target;
	}

	/**
	 * Sets the category.
	 *
	 * @param category
	 *            the new category
	 */
	public void setCategory(IExplorerCategory category) {
		this.category = category;
	}

	/**
	 * Sets the icon i18n key.
	 *
	 * @param iconKey
	 *            the new icon i18n key
	 */
	public void setIconI18nKey(String iconKey) {
		this.iconKey = iconKey;
	}

	/**
	 * Sets the i18n label key.
	 *
	 * @param i18nLabelKey
	 *            the new i18n label key
	 */
	public void setI18nLabelKey(String i18nLabelKey) {
		this.i18nLabelKey = i18nLabelKey;
	}

	/**
	 * Sets the i18n description key.
	 *
	 * @param i18nDescriptionKey
	 *            the new i18n description key
	 */
	public void setI18nDescriptionKey(String i18nDescriptionKey) {
		this.i18nDescriptionKey = i18nDescriptionKey;
	}

	/**
	 * Sets the icon uri.
	 *
	 * @param iconURI
	 *            the new icon uri
	 */
	public void setIconURI(String iconURI) {
		this.iconURI = iconURI;
	}

	/**
	 * Sets the icon.
	 *
	 * @param icon
	 *            the new icon
	 */
	public void setIcon(Resource icon) {
		this.icon = icon;
	}

	/**
	 * Sets the label.
	 *
	 * @param label
	 *            the new label
	 */
	public void setLabel(String label) {
		this.label = label;
	}

	/**
	 * Sets the description.
	 *
	 * @param description
	 *            the new description
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/* (non-Javadoc)
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(IExplorerInfo other) {
		return getLabel().compareTo(other.getLabel());
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AbstractExplorerInfo other = (AbstractExplorerInfo) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}
